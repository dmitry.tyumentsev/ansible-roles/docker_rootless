docker_rootless role
=========

Installs [Docker CE](https://docs.docker.com/engine/) on Debian/Ubuntu in rootless mode.

Based on [Configuring Rootless docker with ansible](https://blog.ianpreston.ca/posts/2023-06-04-rootless-docker.html)

Example Playbook
----------------
```yml
- hosts: docker
  become: true
  roles:
    - docker_rootless
```
